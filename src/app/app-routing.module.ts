import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasicHightlightDirective } from './directives/basic-hightlight/basic-heightlight.directive';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
