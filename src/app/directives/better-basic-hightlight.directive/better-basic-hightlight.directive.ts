import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBetterBasicHightlight]'
})
export class BetterBasicHightlightDirective implements OnInit {
  @Input() defaultColor: string = 'transparent';
  @Input() hightlightColor: string = 'green';
  @HostBinding('style.backgroundColor') backgroundColor: string | undefined;

  constructor() { }

  ngOnInit(): void {
    this.backgroundColor = this.defaultColor;
  }

  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.backgroundColor = this.hightlightColor;
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.backgroundColor = this.defaultColor;
  }
}
